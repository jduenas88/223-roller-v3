@extends('layouts.app')

@section('content')
    <h2 id="character-name">{{ $character->name }}</h2>
    <table id="character-information">
        <tr>
            <td>Created On:</td>
            <td>{{ $character->created_at }}</td>
        </tr>
        <tr>
            <td>Campaign:</td>
            <td>
                <a href="{{ url('campaigns/' . $character->campaign->id) }}">{{ $character->campaign->name }}</a>
            </td>
        </tr>
        <tr>
            <td>Rolls:</td>
            <td>
                <ul id="character-roll-list">
                @foreach($character->rolls as $roll)
                    <li><a class="roll-note-link" title="{{ $roll->notes }}">{{ $roll->roll }} = {{ $roll->results }}</a></li>
                @endforeach
                </ul>
            </td>
        </tr>
    </table>
@endsection