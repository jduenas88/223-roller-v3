@extends('layouts.app')

@section('content')

<form action="{{ action('RollsController@store') }}" method="POST">
    {{ csrf_field() }}
    <div class="form-field">
        <label for="campaign">Select Campaign</label><br>
        <select name="campaign" id="campaign_selector">
            <option value=""></option>
            @foreach($campaigns as $campaign)
                <option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-field">
        <label for="character">Select Character</label><br>
        <select name="character" id="character_selector">
            <option value="">Select Campaign First</option>
        </select>
    </div>
    
    <div class="form-field">
        <label for="lucky_phrase">Lucky Phrase</label><br>
        <input type="text" name="lucky_phrase" id="lucky_phrase">
    </div>

    <div class="form-field">
        <label for="roll">Roll</label><br>
        <input type="text" name="roll" id="roll">
    </div>

    <div class="form-field">
        <label for="notes">Roll Notes</label><br>
        <textarea name="notes" id="roll_notes" cols="20" rows="5"></textarea>
    </div>

    <div class="form-field">
        <input type="submit" value="🎲 Roll" id="roll-button">
    </div>
</form>

@endsection

@section('scripts')
@endsection