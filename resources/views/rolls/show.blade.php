@extends('layouts.app')

@section('content')
    <h2 id="roll-results">You rolled a <br><span>{{ $roll->results }}</span></h2>
    <table id="roll-information">
        <tr>
            <td colspan="2" id="roll-information-header">Roll Information</td>
        </tr>
        <tr>
            <td>Permalink:</td>
            <td><a href="{{ url('rolls/' . $roll->id) }}">{{ url('rolls/' . $roll->id) }}</a></td>
        </tr>
        <tr>
            <td>Campaign:</td>
            <td>
                <a href="{{ url('campaigns/' . $character->campaign->id) }}">{{ $character->campaign->name }}</a>
            </td>
        </tr>
        <tr>
            <td>Character:</td>
            <td>
                <a href="{{ url('characters/' . $roll->character->id) }}">{{ $roll->character->name }}</a>
            </td>
        </tr>
        <tr>
            <td>Roll:</td>
            <td>{{ $roll->roll }}</td>
        </tr>
        <tr>
            <td>Results:</td>
            <td>{{ $roll->results }}</td>
        </tr>
        @if($roll->notes)
            <tr>
                <td>Notes:</td>
                <td><span id="roll-notes">{{ $roll->notes }}</span></td>
            </tr>
        @endif
        <tr>
            <td>Raw Roll Data:</td>
            <td>
                <a href="#showhide" id="raw-data-toggle">Show/Hide</a>
                <ul id="raw-roll-data">
                    <li><strong>Input:</strong> {{ $roll->raw_roll_data('input') }}</li>
                    <li><strong>Equation:</strong> {{ $roll->raw_roll_data('equation') }}</li>
                    <li><strong>Individual Rolls:</strong>
                        <ul>
                        @foreach($roll->raw_roll_data('rolls') as $individual_roll)
                            <li><strong>Notation:</strong> {{ $individual_roll->notation }}</li>
                            <li><strong>Roll Results:</strong>
                                <ul>
                                    @foreach($individual_roll->rolls as $key => $result)
                                        <li><strong>Roll #{{ $key + 1 }}</strong>: {{ $result }}</li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                        </ul>
                    </li>
                    <li><strong>Final Result:</strong> {{ $roll->raw_roll_data('result') }}</li>
                </ul>
            </td>
        </tr>
        <tr id="roll-again">
            <td colspan="2">
                <a href="{{ url('rolls') }}">Roll Again</a>
            </td>
        </tr>
    </table>
@endsection

@section('scripts')
    <script>
        $('#raw-data-toggle').click(function(){
            $('#raw-roll-data').slideToggle();
        });
    </script>
@endsection