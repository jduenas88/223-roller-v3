$(document).ready(function(){
    $('#campaign_selector').change(function(){
        var campaignId = $(this).val();
        var campaignCharacters = getCharactersList(campaignId);
    });
});

function getCharactersList(campaignId){
    if(campaignId.length > 0){
        var request = $.ajax({
            url: "/campaigns/" + campaignId + "/characters",
            method: "GET",
            dataType: "json"
        });

        request.done(function(characters){
            $('#character_selector').find('option').remove();
            $.each(characters, function(key, character){
                $('#character_selector').append("<option value='" + character.id + "'>" + character.name + "</option>");
            })
        });
    }
}