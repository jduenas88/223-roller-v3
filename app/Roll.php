<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roll extends Model
{
    public function character()
    {
        return $this->belongsTo('App\Character');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }

    public function raw_roll_data($key = "input")
    {
        $raw_data = json_decode($this->raw_data);

        return $raw_data->$key;
    }
}
