<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Roll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Nuffle\Nuffle;

class RollsController extends Controller
{
    public function index(Request $request)
    {
        $campaigns = Campaign::where('active', 1)->get();
        $data = ['campaigns' => $campaigns];

        if(isset($request->error)){
            $data['error'] = $request->error;
        }

        return view('rolls.index', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'campaign' => 'required|numeric',
            'character' => 'required|numeric',
            'roll' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->action('RollsController@index', [
                'error' => 'Please make sure you\'ve selected a campaign and character, along with a roll'
            ]);
        }

        $roll = new Roll();
        $results = $this->roll($request->roll);
        $roll->campaign_id = $request->campaign;
        $roll->character_id = $request->character;
        $roll->roll = $request->roll;
        $roll->results = $results->result;
        $roll->raw_data = json_encode($results);
        $roll->notes = $request->notes;
        $roll->save();

        return redirect('rolls/' . $roll->id);
    }

    public function show($id)
    {
        $roll = Roll::findorFail($id);

        return view('rolls.show', [
           'roll' => $roll
        ]);
    }

    protected function roll($roll)
    {
        return Nuffle::roll($roll);
    }
}
