<?php

namespace App\Http\Controllers;

use App\Character;
use Illuminate\Http\Request;

class CharactersController extends Controller
{
    public function show($id){
        $character = Character::findOrFail($id);

        return view('characters.show', [
            'character' => $character
        ]);
    }
}
