<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    public function campaign(){
        return $this->belongsto('App\Campaign');
    }

    public function rolls(){
        return $this->hasMany('App\Roll');
    }
}
