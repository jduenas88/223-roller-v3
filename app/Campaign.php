<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    public function characters()
    {
        return $this->hasMany('App\Character');
    }
}
